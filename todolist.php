<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require 'vendor/autoload.php';
require 'db.php';
$app = new \Slim\App();


?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <link rel="stylesheet" href="style.css" type="text/css" />
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <title></title>
   <style>
  .sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  .sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  .sortable li span { position: absolute; margin-left: -1.3em; }
  </style>
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="jscolor.js"></script>
  <script src="script.js"></script>
</head>

<body>

  <div id="page-wrap">
    <div id="header">
      <h1 style="margin:0 auto;color:green;text-align:center ">Work List</h1>
    </div>

    <div id="main">
      <noscript>This site just doesn't work, period, without JavaScript</noscript>

      <ul id="list" class="ui-sortable " style="margin: 0 auto; width:750px" >
        
  <?php
  $query = "SELECT * FROM todolist order by pos " ; 
  $result = $conn->query($query);
  if ($result) {

    /* fetch object array */
    while ($row = $result->fetch_row()) { 
  ?>
 <li color="1" class="colorBlue" rel="3" data-currPos="<?php echo $row[0]; ?>">
 <?php if($row[2]=='completed'){?>
 <span class="6listitem" id="list_<?php echo $row[0]; ?>" style="background-color: #<?php echo $row[3] ?> ; opacity:0.3 ; text-decoration : line-through" title="Double-click to edit..."><?php echo $row[1]; ?><input id="ids" type = "hidden" value ="<?php echo $row[0]; ?>"/></span>
  <?php
 }
 else{
 ?>
          <span class="6listitem" id="list_<?php echo $row[0]; ?>" data-currID="<?php echo $row[0]; ?>" style="background-color: #<?php echo $row[3] ?>" title="Double-click to edit..."><?php echo $row[1]; ?><input id="ids" type = "hidden" value ="<?php echo $row[0]; ?>"/></span>
 <?php }?>          
          <div class="draggertab tab">

          </div>

          <div class="colortab tab">
           <input class="jscolor" data-currId="<?php echo $row[0]; ?>" style="width : 34px ; opacity:-1 " value="ab2567"/>
          </div>

          
           <div class="deletetab tab"><button data-currId="<?php echo $row[0]; ?>" class='delete'  style="opacity:-1">click me</button></div>
          

          <div class="donetab tab">
            <button data-currId="<?php echo $row[0]; ?>" class='done'  style="opacity:-1">click me</button>
          </div>
        </li>
        <?php
 }
 }
 ?>
          
 </ul>
	  <br />

      <form action="ParisQuery.php" method="post" style="text-align:center;margin: 0 auto; width:250px,">
        <input type="text" id="item" placeholder="Enter Your Task" name="item" style=" display: inline-block;" />
        <input type = "hidden" name="row_order" id="row_order" /> 
        <input type="submit" id="add-new-submit" value="Add" class="button" name="Add"style=" display: inline-block;background-color:green"  />
        </form>
    
      
      <div class="clear"></div>
    </div>
  </div>
 
</body>
</html>
